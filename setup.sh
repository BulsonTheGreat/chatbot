#!/bin/sh

echo "Installing Flask..."
pip install flask || exit 1

echo "Installing Requests..."
pip install requests || exit 1

echo "Upgrading Flask-CORS..."
pip install -U flask-cors || exit 1

echo "Installation complete."

# Zakończ skrypt sukcesem
exit 0
