CREATE DATABASE IF NOT EXISTS chat_db;
USE chat_db;

CREATE TABLE User (
    id_user INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL
);

CREATE TABLE Photo (
    id_photo INT AUTO_INCREMENT PRIMARY KEY,
	path_to_photo VARCHAR(255),
    date DATE NOT NULL,
    user INT,
    FOREIGN KEY (user) REFERENCES User(id_user)
);

CREATE TABLE Messages (
    id_message INT AUTO_INCREMENT PRIMARY KEY,
    user INT,
    message TEXT NOT NULL,
    response TEXT NOT NULL,
    date DATETIME NOT NULL,
    FOREIGN KEY (user) REFERENCES User(id_user)
);

CREATE TABLE Rating (
    id_rating INT AUTO_INCREMENT PRIMARY KEY,
    user INT,
    rating INT CHECK (rating BETWEEN 1 AND 5),
    FOREIGN KEY (user) REFERENCES User(id_user)
);