from flask import Flask, request, render_template, make_response
import requests
import os
import json
from flask_cors import CORS 
from DatabaseHandler import DatabaseHandler

URL = "http://localhost:3000/api/chat"

db_handler = DatabaseHandler(host='127.0.0.1', port='3306', user='root', password='root123', database='chat_db')

def get_messages_history():
    return []


def new_question(question):
    return {
        "role": "user",
        "content": question
    }


def get_json_from_scheme(question):
    messages_history = get_messages_history()
    messages_history.append(new_question(question))

    json_data = {
        "model": {
            "id": "/models/llama-2-70b-chat.bin",
            "name": "Llama 2 70B",
            "maxLength": 12000,
            "tokenLimit": 4000
        },
        "messages": messages_history,
        "key": "",
        "prompt": "You are a helpful and friendly AI assistant. Never lie to me",
        "temperature": 1
    }

    return json.dumps(json_data)


def send_req(question):
    json_data = get_json_from_scheme(question)
    HEADERS = {
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Accept': '*/*'
    }
    print(json_data)
    response = requests.post(URL, json=json.loads(json_data), headers=HEADERS)
    print(response.text)
    return response


frontend_dir = os.path.abspath('./projekt-grupowy-ChatGPT-klient/chatUI')
app = Flask(__name__, template_folder=frontend_dir,
            static_folder=frontend_dir)
CORS(app) 


@app.route('/<string:page_name>/')
def render_static(page_name):
    return render_template(f"/{page_name}.html")


@app.route('/message', methods=['POST'])
def procces_request():
    # value = request.form.get('key', 'default-value')
    # message_data = request.get_data('message')
    # username_data = request.get_data('username')
    userTranslated = request.form['username']
    messageTranslated = request.form['message']
    print(request.values)

    response_from_model = "This is a response to the message"

    # tutaj mozesz wywolac swoja funkcję dla message_data, username_data, response_from_model
    db_handler.addMessage(username=userTranslated, message=messageTranslated, response=response_from_model)
    
    # response_from_model = send_req("Tell me a joke")
    response_to_this_post = make_response("sdjhsjhjdshjds", 200)
    response_to_this_post.headers["Content-Type"] = "text/plain"
    return response_to_this_post

@app.route('/photo', methods=['POST'])
def photo_request():
    # image_data = request.get_data('imageData')
    imageTranslated = request.form['imageData']
    #wywolanie funkcji Dominika do rozpoznawania twarzy, 
    #username = funkcja(image_data);

    # if username:
    #     response_message = imie
    #     status_code = 200
    #     <wywolanie funkcji zapisania zdjecia do bazy danych do sciezki /username>
    # else:
    #     response_message = "not found"
    #     status_code = 200
    #     <jezeli nie znaleziony na razie nic nie zapisujemy, po stronie klienta bedzie ponowne przeslanie zdjecia wraz z imieniem>
    # response_to_this_post = make_response(response_message, status_code)

    # response_to_this_post = make_response("Jan", 200)
    response_to_this_post = make_response("not found", 404)
    return response_to_this_post

@app.route('/new-photo', methods=['POST'])
def new_photo_request():
    # image_data = request.get_data('imageData')
    # username_data = request.get_data('usernameData')
    userTranslated = request.form['usernameData']
    imageTranslated = request.form['imageData']
    db_handler.createUser(username=userTranslated)
    db_handler.addPhoto(username=userTranslated, path_to_photo=imageTranslated)
    #zapisanie zdjecia i nazwy uzytkownika zarowno w folderze jak i bazie danych

    response_to_this_post = make_response("Udalo sie", 200)
    return response_to_this_post


@app.route('/rate', methods=['POST'])
def rate_request():
    # rate_data = request.get_data('rate')
    # username_data = request.get_data('username')
    rateTranslated = request.form['rate']
    userTranslated = request.form['username']

    db_handler.giveRating(username=userTranslated, rating=rateTranslated)
    response_to_this_post = make_response("Udalo sie", 200)
    return response_to_this_post

@app.route('/audio', methods=['POST'])
def audio_request():
    audio_data = request.get_data('audio')

    #uruchomienie funkcji przetwarzajacej format blob na mp3 
    #zapisanie .mp3 jako plik do folderu z nagraniami
    #funckcja przeksztalcajaca nagranie na tekst przekazujac sciezke do nagrania
    #przekazanie odczytanego tekstu jako zapytanie do modelu 
    # response_from_model = send_req("Tell me a joke")

    #zwrocenie odpowiedzi na zapytanie
    response_to_this_post = make_response("To jest odpowiedz na pytanie audio", 200)
    return response_to_this_post


if __name__ == "__main__":
    app.run()

